package com.telsoft.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabAppCiApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabAppCiApplication.class, args);
    }

}
